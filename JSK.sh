#!/usr/bin/python

'''Copyright (c) 2017 Pasquale De Rose A.K.A. J-Lemon
This software is provided 'as-is', without any express or implied warranty. In no event will the authors be held liable for any damages arising from the use of this software.

Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

    1. The origin of this software must not be misrepresented; you must not claim that you wrote the original software. If you use this software in a product, an acknowledgment in the product documentation would be appreciated but is not required.

    2. Altered source versions must be plainly marked as such, and must not be misrepresented as being the original software.

    3. This notice may not be removed or altered from any source distribution.'''

import os, subprocess, sys

def install():
  try:
    subprocess.call(["ldoc"])
    print("  [JSK] ldoc is Already Installed")
  except:
    print("  [JSK] Installing ldoc")
    subprocess.call(["luarocks", "install", "ldoc"])
    print("  [JSK] ldoc installed")

  if os.path.isdir("./.tools/emscripten") == False:
    print("  [JSK] Installing emscripten")
    os.chdir("./.tools/")
    subprocess.call(["git", "clone", "https://github.com/kripken/emscripten/"])
    os.chdir("..")
    print("  [JSK] emscripten installed")
  else:
    print("  [JSK] emscripten Is Already Installed")
    
  if os.path.isdir("./src") == False:
    os.mkdir("./src")
  if os.path.isdir("./src/libs") == False:
    os.mkdir("./src/libs")
  if os.path.isdir("./src/libs/gooi") == False:
    os.chdir("./src/libs/")
    subprocess.call(["git", "clone", "https://github.com/tavuntu/gooi"])
    
    
def build():
  print("\t[JSK] Building game")
  if os.path.isfile("./game.js") == True:
    os.remove("./game.js")
  if os.path.isfile("./game.data") == True:
    os.remove("./game.data")
  subprocess.call(["python", "./.tools/emscripten/tools/file_packager.py", "game.data", "--preload", "./src@/", "--js-output=game.js"])
  print("  [JSK] Done")
  
def run():
  import SimpleHTTPServer
  import SocketServer

  PORT = 8000

  Handler = SimpleHTTPServer.SimpleHTTPRequestHandler
  httpd = SocketServer.TCPServer(("", PORT), Handler)
  print("  [JSK] running")
  httpd.serve_forever()

def help():
  print("JAM Survial Kit\nWritten with haste by Pasquale De Rose A.K.A. J-Lemon")
  print("Available options:")
  print("  install => install the dependencies and the enviroment into the computer")
  print("  build => build the project")
  print("  run => run the project on local host ip with port 8000")

try:
  arg = str(sys.argv[1])
  if arg == "install":
    install()
  elif arg == "build":
    build()
  elif arg == "run":
    run()
  else:
    help()
except:
  help()
