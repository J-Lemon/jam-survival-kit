--[[           -- Jam Survival Kit Libs --
        Copyright (c)Pasquale De Rose, All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3.0 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.]]

require("Rect")

Entity = {}
Entity.__index = Entity

function Entity.create(x, y, width, height, update_fn, draw_fn)
  local tmp_entity = {}
  setmetatable(tmp_entity, Entity)
  
  tmp_entity.Update = update_fn or nil
  tmp_entity.Draw   = draw_fn or nil

  tmp_entity.center_x = x + (width /2)
  tmp_entity.center_y = y + (height/2)
  
  tmp_entity.collision_rects = {}
  tmp_entity.collision_rects["top"]    = Rect.create(x, y, width, height/2)
  tmp_entity.collision_rects["bottom"] = Rect.create(x, tmp_entity.center_y, width, height/2)
  tmp_entity.collision_rects["left"]   = Rect.create(x, y, width/2, height)
  tmp_entity.collision_rects["right"]  = Rect.create(tmp_entity.center_x, y, width/2, height)
  
  return tmp_entity
end

function Entity:Set_Position(x, y)
  self.center_x = x + (self.width /2)
  self.center_y = y + (self.height/2)
  
  self.collision_rects["top"].x    = x
  self.collision_rects["top"].y    = y
  self.collision_rects["bottom"].x = x
  self.collision_rects["bottom"].y = self.center_y
  self.collision_rects["left"].x   = x
  self.collision_rects["left"].y   = y
  self.collision_rects["right"].x  = self.center_x
  self.collision_rects["right"].y  = y
end

function Entity:Move(x, y)
  for rect in self.collision_rects do
    rect.x = rect.x + x
    rect.y = rect.y + y
  end
end


return Entity
