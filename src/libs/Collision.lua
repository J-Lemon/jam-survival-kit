--[[           -- Jam Survival Kit Libs --
        Copyright (c)Pasquale De Rose, All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3.0 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.]]

Collision = {}

function Collision.Check_Rect(a, b)
  return a.x < b.x + b.width  and
         b.x < a.x + a.width  and
         a.y < b.y + b.height and
         b.y < a.y + a.height
end

function Collision.Check_Entity(a, b)
  for direction, rect_a in pairs(a.collision_rects) do
    for rect_b in b.collision_rects do
      if Check_Rect(rect_a, rect_b) == true then
        return direction
      end
    end
  end
  return nil
end
    
return Collision
