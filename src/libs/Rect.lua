--[[           -- Jam Survival Kit Libs --
        Copyright (c)Pasquale De Rose, All rights reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 3.0 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.]]

Rect = {}
Rect.__index = Rect

function Rect.create(x, y, width, height)
   local rect = {}
   setmetatable(rect, Rect)
   rect.x = x or 0
   rect.y = y or 0
   rect.width = width or 0
   rect.height = height or 0
   return rect
end

return Rect
